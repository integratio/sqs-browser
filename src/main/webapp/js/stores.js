var AppStore = {
  status : "INITIALIZED",
  selectedQueue: null
};
MicroEvent.mixin( AppStore );  

AppDispatcher.register(function(payload) {
  switch (payload.eventName) {
    case "status.changed":
      console.log("Updating status from " + AppStore.status + " to " + payload.newState);
    AppStore.status = payload.newState;
    AppStore.trigger( 'status.changed' );
    break;
  }
  return true;
});

AppDispatcher.register(function(payload) {
  switch (payload.eventName) {
    case "error.occurred":
    AppStore.status = payload.newState;
    AppStore.trigger( 'status.changed' );
    break;
  }
  return true;
});

var RegionStore = {
  regions : [],

  getAll: function() {
    return this.regions;
  }
};
MicroEvent.mixin( RegionStore );  

AppDispatcher.register(function(payload) {
  switch (payload.eventName) {
    case "regions.loaded":
      RegionStore.regions = payload.regions;
      RegionStore.trigger('regions.loaded');
      break;
  }
  return true;
});

AppDispatcher.register(function(payload) {
  switch (payload.eventName) {
    case "region.selected":
      RegionStore.selectedRegion = payload.selectedRegion;
      RegionStore.trigger('region.selected');
      break;
  }
  return true;
});

var QueueStore = {
  queues : [],
  loading: false,

  clear: function() {
    this.queues = [];
  },

  getAll: function() {
    return this.queues;
  }

};
MicroEvent.mixin( QueueStore );  

AppDispatcher.register(function(payload) {
  switch (payload.eventName) {
    case "queues.received":
      console.log("Updating queues.received with " + payload.queues);
      QueueStore.loading = false;
      QueueStore.queues = payload.queues;
      QueueStore.trigger('queues.changed');
      break;
    case "queues.loading":
      QueueStore.loading = true;
      QueueStore.trigger('queues.changed');
      break;
  }
  return true;
});

var MessageStore = {
  messages : [],
  loading: false,

  getAll: function() {
    return this.messages;
  }
};
MicroEvent.mixin( MessageStore );  

AppDispatcher.register(function(payload) {
  switch (payload.eventName) {
    case "messages.received":
      MessageStore.loading = false;
      MessageStore.messages = payload.messages;
      MessageStore.trigger('messages.changed');
      break;
    case "messages.loading":
      MessageStore.loading = true;
      MessageStore.trigger('messages.changed');
    break;
  }
  return true;
});

var ErrorStore = {
  errors : [],
  hasError: false,

  getAll: function() {
    return this.errors;
  }
};
MicroEvent.mixin( ErrorStore );  

AppDispatcher.register(function(payload) {
  switch (payload.eventName) {
    case "error.occurred":
      ErrorStore.hasError = true;
      ErrorStore.messages = payload.errorMessage;
      ErrorStore.trigger('error.occurred');
      break;
  }
  return true;
});
