var CredentialsAction = {
  connect: function(accessKey, secretKey) {
    AppDispatcher.dispatch({  eventName: 'status.changed', newState: 'CREDENTIALS_ENTERED' });
    $.getJSON( "/connect", { accessKey: accessKey, secretKey: secretKey })
    .done(function( data ) {
      console.log('Connected!');
      AppDispatcher.dispatch({  eventName: 'status.changed', newState: 'CREDENTIALS_SENT' });
      RegionAction.loadRegions();
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      console.error('Connection failed!');
      AppDispatcher.dispatch({  eventName: 'error.occurred', newState: 'CREDENTIALS_FAILED' });
    });
  }
};

var RegionAction = {
  loadRegions: function() {
    var url = '/regions';
    AppDispatcher.dispatch({eventName: 'status.changed', newState: 'REGIONS_LOADING' });
    $.getJSON(url, function(result){
      if(!result || !result.regions || !result.regions.length) {
        console.error('Empty region list received: ' + result);
        AppDispatcher.dispatch({eventName: 'status.changed', newState: 'REGIONS_FAILED' });
      } else {
        console.log('Retrieved regions %j', result);
        AppDispatcher.dispatch({eventName: 'status.changed', newState: 'REGIONS_RECEIVED' });
        AppDispatcher.dispatch({eventName: 'regions.loaded', regions: result.regions });
      }
    });
  },

  selectRegion: function(selected) {
    $.post( "/region", { region: selected } )
    .done(function(data) {
      console.log( "Selected region propagated to backend");
      AppDispatcher.dispatch({eventName: 'status.changed', newState: 'REGION_SELECTED' });
      AppDispatcher.dispatch({eventName: 'region.selected', selectedRegion: selected});
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      console.error('Failed to propagate region: ' + selected);
      AppDispatcher.dispatch({eventName: 'status.changed', newState: 'REGIONS_SELECT_FAILED' });
    });
  }
};

var QueueAction = {
  getQueueList: function() {
    AppDispatcher.dispatch({eventName: 'queues.loading'});
    console.log('Retrieving queue list');
    var url = '/queues';
    $.getJSON(url)
      .done(function(result){
      if(!result || !result.length) {
        console.error('Empty queue list received: ' + result);
        AppDispatcher.dispatch({eventName: 'status.changed', newState: 'QUEUES_FAILED' });
      } else {
        console.log('Retrieved queues' + result);                
        AppDispatcher.dispatch({eventName: 'status.changed', newState: 'QUEUES_RECEIVED' });
        AppDispatcher.dispatch({eventName: 'queues.received', queues: result});
      }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
      console.error("Failed to get queue list: " + textStatus + " " + errorThrown);
      AppDispatcher.dispatch({eventName: 'error.occurred', newState: 'QUEUES_FAILED' });
    });
  },

  getMessageList: function(queueUrl) {
    AppDispatcher.dispatch({eventName: 'status.changed', newState: 'QUEUE_SELECTED' });
    AppDispatcher.dispatch({eventName: 'messages.loading'});
    console.log("Retrieving queue messages for " + queueUrl);
    $.getJSON( "/queue/messages", { queueUrl: queueUrl}, function(result) {
      if(!result || !result.length) {
        console.error('Empty queue message list received: ' + result);
        AppDispatcher.dispatch({eventName: 'status.changed', newState: 'QUEUE_MESSAGES_EMPTY' });
      } else {
        console.log('Retrieved queue messages');
        AppDispatcher.dispatch({eventName: 'status.changed', newState: 'QUEUE_MESSAGES_RECEIVED' });
        AppDispatcher.dispatch({eventName: 'messages.received', messages: result});
      }
    });
  }
};
