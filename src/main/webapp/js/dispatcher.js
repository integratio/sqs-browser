var AppDispatcher = {
  callbacks : [], 

  register: function(func) {
    console.log("Registered callback");
    this.callbacks.push(func);
  },

  dispatch: function(payload) {
    console.log("Dispatched event " + payload.eventName);
    this.callbacks.map(function(func) {
      console.log("Invoking callback");
      func(payload);
    });
  }
};
