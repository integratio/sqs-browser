var CredentialsRegion = React.createClass({
  render: function() {
    console.log('Rendering credentials form');
    return (<div className="row">
      <Icon active={this.props.active} />
      <CredentialsForm connect={this.connect} />
    </div>);
  }
});

var CredentialsForm = React.createClass({
  handleSubmit: function(event) {
    event.preventDefault();
    console.log('Credentials form submitted');
    var accessKey = this.refs.accessKey.getDOMNode().value.trim();
    var secretKey = this.refs.secretKey.getDOMNode().value.trim();
    if (!accessKey || !secretKey) {
      console.log('Ignoring empty form submit');
      return;
    }
    CredentialsAction.connect(accessKey, secretKey);
  },
  render: function() {
    console.log('Rendering credentials form');
    return (<div className="eight columns" id="credentialsDiv">
      <h4>Credentials</h4>
      <div id="credentialsMain">
        <input className="u-full-width" ref="accessKey" id="accessKeyInput" type="text" defaultValue="" placeholder="Access Key"></input>
        <input className="u-full-width" ref="secretKey" id="secretKeyInput" type="text" defaultValue="" placeholder="Secret Key"></input>
        <input className="button-primary" id="connectBtn" value="Connect" type="submit" onClick={this.handleSubmit}></input>
      </div>
    </div>);
  }
});

var RegionSelector = React.createClass({
  getRegionState: function() {
    return {regions: RegionStore.regions};
  },
  getInitialState: function() {
    return this.getRegionState();
  },
  componentDidMount: function() {
    RegionStore.bind( 'regions.loaded', this.regionsLoaded);
  },
  componentWillUnmount: function() {  
    RegionStore.unbind( 'regions.loaded', this.regionsLoaded);
  },
  regionsLoaded: function() {
    this.setState(this.getRegionState());
  },
  render: function() {
    console.log('Rendering region selector');
    return (<div className="row">
              <Icon active={this.props.active} />
              <RegionInput regions={this.state.regions} />
            </div>);
  }
});

var RegionInput = React.createClass({
  handleSubmit: function(event) {
    event.preventDefault();
    var selected = this.refs.regionsSelect.getDOMNode().value;
    console.log('Selected region: ' + selected);
    RegionAction.selectRegion(selected);
  },
  render: function() {
    var options = this.props.regions.map(function(r){
      return <option key={r} value={r}>{r}</option>;
    });

    return (<div className="eight columns" id="regionsDiv">
      <h4>Select Region</h4>
      <div id="regionsMain">
        <select id="regionsSelect" defaultValue='eu-west-1' className="u-full-width" ref="regionsSelect">
          {options}
        </select>
        <input className="button-primary" id="regionBtn" value="Go" type="submit" onClick={this.handleSubmit} />
      </div>
    </div>);
  }
});

var QueueTable = React.createClass({
  getQueueState: function() {
    return {queues: QueueStore.getAll(), loading: QueueStore.loading};
  },
  getInitialState: function() {
    return this.getQueueState(); 
  },
  componentDidMount: function() {  
    QueueStore.bind( 'queues.changed', this.queuesChanged );
  },
  componentWillUnmount: function() {  
    QueueStore.unbind( 'queues.changed', this.queuesChanged );
  },
  queuesChanged: function() {
    this.setState(this.getQueueState());
  }, 
  render: function() {
    console.log('Rendering queue table');

    var rows;
    if (this.state.loading === true) {
      rows = <tr><td>Loading...</td></tr>;    
    } else if (this.state.queues.length === 0) {
      rows = <tr><td>No queues found</td></tr>;    
    } else {
      var self = this;    
      rows = this.state.queues.map(function(q) {
        return <QueueRow key={q} queueUrl={q} />;
      });
    }

    return (<div>
      <div className="row">
        <Icon active={this.props.active} />
        <div className="eight columns" id="queuesDiv">
          <h4>Select Queue</h4>
          <table id="queuesTbl" className="u-full-width">
            <tbody>
              {rows}
            </tbody>
          </table>
        </div>              
      </div>
    </div>);
  }
});

var QueueRow = React.createClass({
  handleClick: function(event) {
    event.preventDefault();
    QueueAction.getMessageList(this.props.queueUrl);
  },
  render: function() {
    return <tr key={this.props.queueUrl} onClick={this.handleClick}><td><a>{this.props.queueUrl}</a></td></tr>;
  }
});

var QueueMessages = React.createClass({
  getMessageState: function() {
    return {messages: MessageStore.getAll(), loading: MessageStore.loading};
  },
  getInitialState: function() {
    return this.getMessageState(); 
  },
  componentDidMount: function() {  
    MessageStore.bind('messages.changed', this.messagesChanged);
  },
  componentWillUnmount: function() {  
    MessageStore.unbind('messages.changed', this.messagesChanged);
  },
  messagesChanged: function() {
    console.log("Event captured: messagesChanged");
    this.setState(this.getMessageState());
  }, 

  render: function() {
    console.log('Rendering queue message');

    var rows;
    if (this.state.loading === true) {
      rows = <tr><td>Loading...</td></tr>;    
    } else if (this.state.messages.length === 0) {
      rows = <tr><td>No messages found</td></tr>;    
    } else {
      rows = this.state.messages.map(function(m) {
        return <QueueMessageRow key={m.id} messageId={m.id} messageBody={m.body} messageLimit={75} />;
      });
    }

    return (<div className="row">
      <Icon active={this.props.active} />
      <div className="ten columns" id="queueMessagesDiv">
        <h4>Messages</h4>
        <table id="queueMessagesTbl" className="u-full-width">
          <tbody>
            {rows}
          </tbody>
        </table>
      </div>
    </div>);
  }
});

var QueueMessageRow = React.createClass({
  getInitialState: function() {
    return {clicked: false};
  },
  handleClick: function(event) {        
    this.setState({ clicked: !this.state.clicked });
  },
  render: function() {
    var body = this.props.messageBody;
    var limit = this.props.messageLimit;
    var tooLong = body && body.length > limit;
    var clickedMore = this.state.clicked;

    if (!clickedMore && tooLong) {
      console.log("Too long, cutting down to " + limit + " characters");
      body = body.substring(0,limit) + "... ";
    }
    var buttonText = this.state.clicked ? " less" : "more";
    var toggleButton = tooLong ? <span><br /><a onClick={this.handleClick}>{buttonText}</a></span> : null;

    return <tr><td>{this.props.messageId}</td><td>{body}{toggleButton}</td></tr>;
  }
});


var Icon = React.createClass({
  render: function() {
    var icon;
    if (this.props.active) {
      icon = <i className="flaticon-circle10">&nbsp;</i>;
    } else {
      icon = <i className="flaticon-check2">&nbsp;</i>;
    }
    return (<div className="one column">
      {icon}
    </div>);
  }
});

var SQSBrowser = React.createClass({
  getAppState: function() {
    return {status: AppStore.status, selectedQueue: AppStore.selectedQueue, hasError: ErrorStore.hasError};
  },
  getInitialState: function() {
    return this.getAppState();
  },
  componentDidMount: function() {  
    RegionStore.bind( 'region.selected', this.regionSelected );
    ErrorStore.bind( 'error.occurred', this.errorOccurred );
    AppStore.bind( 'status.changed', this.stateChanged );
  },
  componentWillUnmount: function() {  
    RegionStore.unbind( 'region.selected', this.regionSelected );
    AppStore.unbind( 'status.changed', this.stateChanged );
  },
  errorOccurred: function() {
    this.setState(this.getAppState());
  },
  regionSelected: function() {
    QueueAction.getQueueList();
  },
  stateChanged: function() {
    this.setState(this.getAppState());
  },
  render: function() {
    var errorsActive = this.state.hasError;
    var credentialsActive = this.state.status == 'INITIALIZED';
    var regionsActive = this.state.status.startsWith('REGION') || this.state.status.startsWith('QUEUE');
    var queuesActive = this.state.status == 'REGION_SELECTED' || this.state.status.startsWith('QUEUE');
    var messagesActive = this.state.status == 'QUEUE_SELECTED' || this.state.status == 'QUEUE_MESSAGES_RECEIVED';
    var queueSelected = this.state.status == 'QUEUE_SELECTED';
    return (<div id="container" className="container">
      <h3>SQS Browser</h3>
      <CredentialsRegion active={credentialsActive} />
      {regionsActive ? <RegionSelector active={!queuesActive} /> : null}
      {queuesActive ? <QueueTable active={!queueSelected} /> : null}
      {messagesActive ?  <QueueMessages />  : null}
      {errorsActive ? <h1>ERROR</h1> : null}
    </div>);
  },
});

React.render(<SQSBrowser />, document.getElementById('sqs-browser-main'));
