package uk.integratio.cloud.aws;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import uk.integratio.cloud.aws.model.Connect;
import uk.integratio.cloud.aws.model.Message;
import uk.integratio.cloud.aws.model.QueueMessage;
import uk.integratio.cloud.aws.model.RegionList;
import uk.integratio.cloud.aws.service.SQSService;

@Controller
@EnableAutoConfiguration
public class SQSController {

    private static final Logger logger = LoggerFactory
            .getLogger(SQSController.class);

    private static final int MAX_MESSAGES = 10;

    @Autowired
    private SQSService sqsService;

    @RequestMapping(value = "/region", method = RequestMethod.POST)
    public @ResponseBody void selectRegion(
            @RequestParam(value = "region") String regionChoice) {
        logger.info("/regions: {}", regionChoice);
        sqsService.setRegion(regionChoice);
    }

    @RequestMapping(value = "/regions", method = RequestMethod.GET)
    public @ResponseBody RegionList regions() {
        logger.info("/regions");
        RegionList regions = new RegionList();
        try {
            List<String> regionList = sqsService.getRegions();
            regions.setRegions(regionList);
            regions.setResponseStatus(Message.Status.SUCCESS);
        } catch (Exception e) {
            logger.error("Regions request failed", e);
            regions.setResponseStatus(Message.Status.FAILURE);
            regions.setResponseMessage(e.getMessage());
        }
        return regions;
    }

    @RequestMapping(value = "/queues", method = RequestMethod.GET)
    public @ResponseBody List<String> queues() {
        logger.info("/queues");
        List<String> queueList = sqsService.listQueues();
        return queueList;
    }

    @RequestMapping(value = "/queue", method = RequestMethod.GET)
    public @ResponseBody Map<String, String> queues(
            @RequestParam(value = "queueUrl") String queueUrl) {
        logger.info("/queue: {}", queueUrl);
        Map<String, String> queueDetails = sqsService.getQueueDetails(queueUrl);
        return queueDetails;
    }

    @RequestMapping(value = "/queue/messages", method = RequestMethod.GET)
    public @ResponseBody List<QueueMessage> messages(
            @RequestParam(value = "queueUrl") String queueUrl) {
        logger.info("/queue/messages: {}", queueUrl);
        List<QueueMessage> messages = sqsService.getQueueMessages(queueUrl,
                MAX_MESSAGES);
        return messages;
    }

    @RequestMapping(value = "/connect", method = RequestMethod.GET)
    public @ResponseBody Connect connect(
            @RequestParam(value = "accessKey") String accessKey,
            @RequestParam(value = "secretKey") String secretKey) {
        logger.debug("/connect: {} {}", accessKey, secretKey);
        Connect connect = new Connect();
        connect.setAccessKey(accessKey);
        connect.setSecretKey(secretKey);

        try {
            sqsService.connect(connect);
            connect.setResponseStatus(Message.Status.SUCCESS);
        } catch (Exception e) {
            logger.error("Connection request failed", e);
            connect.setResponseStatus(Message.Status.FAILURE);
            connect.setResponseMessage(e.getMessage());
        }

        return connect;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SQSController.class, args);
    }
}
