package uk.integratio.cloud.aws;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import java.awt.Desktop;
import java.net.URI;

@Configuration
@ComponentScan
public class Application {
    
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.setHeadless(false);
        app.run(args);
        
        if (args != null && args.length == 1 && !"--noBrowser".equals(args[0])) { 
          try { 
            Desktop desktop = Desktop.getDesktop();
            desktop.browse(new URI("http://localhost:8080"));
          } catch (Exception e) {
            System.out.println("Could not open up desktop browser");
            e.printStackTrace();
          }
        }
    }
}
