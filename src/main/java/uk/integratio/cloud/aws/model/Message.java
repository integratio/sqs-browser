package uk.integratio.cloud.aws.model;

public class Message {
    protected String responseCode = "";
    protected String responseMessage = "";
    private Status responseStatus = Status.UNKNOWN;

    public enum Status {
        SUCCESS, FAILURE, UNKNOWN
    };

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Status getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Status responseStatus) {
        this.responseStatus = responseStatus;
    }

}
