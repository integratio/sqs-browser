package uk.integratio.cloud.aws.model;

import java.util.List;

public class RegionList extends Message {
    private List<String> regions;

    public List<String> getRegions() {
        return regions;
    }

    public void setRegions(List<String> regions) {
        this.regions = regions;
    }

}
