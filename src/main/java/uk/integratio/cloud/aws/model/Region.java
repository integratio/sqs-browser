package uk.integratio.cloud.aws.model;

import com.amazonaws.services.sqs.model.Message;

public class Region extends Message {

    private static final long serialVersionUID = 2341080748163369430L;

    private String region;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

}
