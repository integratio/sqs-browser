package uk.integratio.cloud.aws.model;

import java.io.Serializable;

public class QueueMessage implements Serializable {

    private static final long serialVersionUID = 2676165570086233895L;

    private String body;
    private String id;

    public void setBody(String body) {
        this.body = body;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public String getId() {
        return id;
    }

}
