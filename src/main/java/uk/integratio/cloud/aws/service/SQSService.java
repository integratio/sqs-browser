package uk.integratio.cloud.aws.service;

import java.util.List;
import java.util.Map;

import uk.integratio.cloud.aws.model.Connect;
import uk.integratio.cloud.aws.model.QueueMessage;

public interface SQSService {
    public void connect(Connect connect);

    public List<String> getRegions();

    public void setRegion(String regionChoice);

    public List<String> listQueues();

    public Map<String, String> getQueueDetails(String queueUrl);

    public List<QueueMessage> getQueueMessages(String queueUrl,
            int maxNumberofMessages);
}
