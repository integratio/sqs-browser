package uk.integratio.cloud.aws.service;

public class SQSServiceException extends RuntimeException {

    private static final long serialVersionUID = 2041640199826680220L;

    public SQSServiceException(String message) {
        super(message);
    }

}
