package uk.integratio.cloud.aws.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.regions.Regions;
import com.amazonaws.regions.ServiceAbbreviations;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;

import uk.integratio.cloud.aws.model.Connect;
import uk.integratio.cloud.aws.model.QueueMessage;

@Component
public class SQSServiceImpl implements SQSService {

    private AmazonSQS sqs;

    public void connect(Connect connect) {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(
                connect.getAccessKey(), connect.getSecretKey());
        sqs = new AmazonSQSClient(awsCreds);
    }

    public List<String> getRegions() {
        List<String> regionList = new ArrayList<String>();
        for (Regions r : Regions.values()) {
            regionList.add(r.getName());
        }
        return regionList;
    }

    public void setRegion(String regionChoice) {
        Region region = RegionUtils.getRegion(regionChoice);

        if (region.isServiceSupported(ServiceAbbreviations.SQS)) {
            sqs.setRegion(region);
        } else {
            throw new SQSServiceException(
                    "SQS is not supported for this region");
        }
    }

    public List<String> listQueues() {
        List<String> queueUrls = sqs.listQueues().getQueueUrls();
        return queueUrls;
    }

    public Map<String, String> getQueueDetails(String queueUrl) {
        GetQueueAttributesRequest request = new GetQueueAttributesRequest();
        request.setQueueUrl(queueUrl);
        GetQueueAttributesResult attributes = sqs.getQueueAttributes(request);
        return attributes.getAttributes();
    }

    public List<QueueMessage> getQueueMessages(String queueUrl,
            int maxNumberOfMessages) {
        ReceiveMessageRequest request = new ReceiveMessageRequest();
        request.setQueueUrl(queueUrl);
        request.setMaxNumberOfMessages(maxNumberOfMessages);
        ReceiveMessageResult response = sqs.receiveMessage(request);

        List<Message> messages = response.getMessages();
        List<QueueMessage> queueMessages = new ArrayList<QueueMessage>();
        for (Message m : messages) {
            QueueMessage queueMessage = new QueueMessage();
            queueMessage.setBody(m.getBody());
            queueMessage.setId(m.getMessageId());
            queueMessages.add(queueMessage);
        }
        return queueMessages;
    }
}
