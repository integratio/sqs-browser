package uk.integratio.cloud.aws;

import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTest {
 
    private Collection collection;
 
    @BeforeClass
    public static void oneTimeSetUp() {
      Application.main(new String[] { "--noBrowser"});
    }
 
    @AfterClass
    public static void oneTimeTearDown() {
    }
 
    @Before
    public void setUp() {
    }
 
    @After
    public void tearDown() {
    }

    private WebDriver getDriver() {
        return new FirefoxDriver();
    }
 
    @Test
    public void testEndToEnd() {
        System.out.println("Starting end-to-end test");

        WebDriver driver = getDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        
        driver.get("http://localhost:8080/");
        System.out.println("Page title is: " + driver.getTitle());
      
        /* send credentials */ 
        WebElement accessKey= driver.findElement(By.id("accessKeyInput"));
        accessKey.sendKeys("AKIAJWFMV5XPWOOYEZVA");
        WebElement secretKey = driver.findElement(By.id("secretKeyInput"));
        secretKey.sendKeys("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");
        WebElement connectBtn = driver.findElement(By.id("connectBtn"));
        connectBtn.click();

        /* wait until region selector appears */
        WebElement regionSelectorElement = (new WebDriverWait(driver, 30))
          .until(ExpectedConditions.presenceOfElementLocated(By.id("regionsSelect")));
        Select regionSelector = new Select(regionSelectorElement);
        regionSelector.selectByValue("eu-west-1");
        WebElement regionButton = driver.findElement(By.id("regionBtn"));
        regionButton.click();
       
        /* wait until queue appears */
        WebElement queueLink = (new WebDriverWait(driver, 60))
          .until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@id='queuesTbl']//tbody/tr/td/a")));
        queueLink.click();

        /* wait until messages appears */
        WebElement queueMessageTable = (new WebDriverWait(driver, 30))
          .until(ExpectedConditions.presenceOfElementLocated(By.id("queueMessagesTbl")));

        WebElement moreLink = (new WebDriverWait(driver, 60))
          .until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@id='queueMessagesTbl']//tbody/tr/td[2]/span/a")));
        moreLink.click();
        
        driver.quit();

        System.out.println("Finished end-to-end test");
    }
 
}
